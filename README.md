# Docker MariaDB Backup Tool
Dockerised project to allow backing up (full and incremental) databases with MariaDB's Mariabackup (similar to Xtrabackup/Innobackupex). Also allow restoring backups.

## Running backups
This is intended to be called from a cronjob.

You need to mount the MariaDB data dir from the host to `/var/lib/mysql` and the backups directory to `/backups_data`. See example commands below

```bash
# Full backup with all default options
sudo docker run --network host -v ~/backups-test/backups:/backups_data -v /var/lib/mysql:/var/lib/mysql registry.gitlab.com/violuke/docker-mariadb-backup-tool:latest backup --user=SET_ME --password=SET_ME --host=127.0.0.1

# Full backup every 10 minutes
sudo docker run --network host -v ~/backups-test/backups:/backups_data -v /var/lib/mysql:/var/lib/mysql registry.gitlab.com/violuke/docker-mariadb-backup-tool:latest backup --user=SET_ME --password=SET_ME --host=127.0.0.1 --fullBackupAgainAfter="10 minutes"

# Full backup of only "front_end" database
sudo docker run --network host -v ~/backups-test/backups:/backups_data -v /var/lib/mysql:/var/lib/mysql registry.gitlab.com/violuke/docker-mariadb-backup-tool:latest backup --user=SET_ME --password=SET_ME --host=127.0.0.1 --databases="my_main_database"
```

| Option | Default Value | Notes |
| ------ | ------------- | ----- |
| `user` | None, required option | A user to login to the database. |
| `password` | None, required option | The password for the user specified in `user`. |
| `host` | None, required option | The host database to connect to. |
| `fullBackupsToKeep` | `10` | Number of full backups to keep when cleaning up. |
| `fullBackupAgainAfter` | `1 day` | How long we should make incremental backups based on a full backup for, before swapping to a new full backup. User a number and then a time value, e.g. `1 week`, `8 hours`, etc. |
| `databases` | All databases | Defaults to all databases, but you can specify in the [same format as Mariadbackup accepts](https://mariadb.com/kb/en/library/mariabackup-options/#-databases), `database[.table][ database[.table] ...]`, so `example.table1 example2` |
| `tool` | `mariabackup` | The tool you'd like to use for the backups. You can choose from `mariadbackup` and `xtrabackup`. |
| `ftwrl-wait-timeout` | `null` | This will be passed directly though to the backup tool, see [docs](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#cmdoption-xtrabackup-ftwrl-wait-timeout). |

## Restoring backups
```bash
sudo docker run -it -v ~/backups-test/backups/some_date:/backups_data -v /var/lib/mysql:/var/lib/mysql registry.gitlab.com/violuke/docker-mariadb-backup-tool:latest restore --incremental=somedate
```

## Creating a user specifically for backups
This should just be considered an example, but you can use something like the following to create a backup user
```sql
CREATE USER 'backups'@'localhost' IDENTIFIED BY 'SOMETHING SUPER SECURE';
Grant RELOAD, LOCK TABLES, REPLICATION CLIENT, SUPER, PROCESS On *.* TO 'backups'@'localhost';
FLUSH PRIVILEGES;
```
<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class BackupCommand extends Command {
	// Configurable options
	private $user = null;
	private $password = null;
	private $host = null;
	private $fullBackupsToKeep = 10;
	private $fullBackupAgainAfter = '1 day';
	private $databases = null;
	private $tool = 'mariabackup';
	private $ftwrlWaitTimeout = null;

	const TOOL_OPTIONS = ['mariabackup', 'xtrabackup'];

	// Hardcoded options
	const BACKUPS_DIR = '/backups_data'; // Don't change this without checking the dockerfile and other commands
	const MARIADB_DATA_DIR = '/var/lib/mysql'; // Don't change this without checking the dockerfile and other commands

	// Internal variables
	private $fullBackupAgainAfterSeconds = null;
	private $mostRecentFullBackupDir = null;

	/** @var SymfonyStyle */
	private $io = null;

	protected function configure() {
		$this
			->setName('backup')
			->setDescription('Creates a new backup (full or incremental, depending on the settings passed in).')
			->addOption('user', null, InputOption::VALUE_REQUIRED, 'Backup database user')
			->addOption('password', null, InputOption::VALUE_REQUIRED, 'Backup database password')
			->addOption('host', null, InputOption::VALUE_REQUIRED, 'Backup database host')
			->addOption('fullBackupsToKeep', null, InputOption::VALUE_REQUIRED, 'Number of full backups to keep, defaults to ' . $this->fullBackupsToKeep . '.')
			->addOption('fullBackupAgainAfter', null, InputOption::VALUE_REQUIRED, 'Time after which a full backup should be done again (otherwise we\'ll do incremental backups), defaults to ' . $this->fullBackupAgainAfter . '.')
			->addOption('databases', null, InputOption::VALUE_REQUIRED, 'Override which databases are backed up.')
			->addOption('tool', null, InputOption::VALUE_REQUIRED, 'Set the tool to be used for backups')
			->addOption('ftwrl-wait-timeout', null, InputOption::VALUE_REQUIRED, 'Set the option --ftwrl-wait-timeout')
        ;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$this->io = new SymfonyStyle($input, $output);

		$this->io->title('Starting new backup');

		$this->handleOptions($input);

		if ($this->shouldWeDoAFullBackup()) {
			if ($this->doFullBackup() !== 0) {
				throw new \Exception('Full backup failed');
			}
		} else {
			if ($this->doIncrementalBackup() !== 0) {
				throw new \Exception('Incremental backup failed');
			}
		}

		$this->cleanup();
	}

	private function handleOptions(InputInterface $input) {
		// User
		$this->user = $input->getOption('user');
		if (is_null($this->user)) {
			throw new \Exception('You must specify a database user.');
		}

		// Password
		$this->password = $input->getOption('password');
		if (is_null($this->password)) {
			throw new \Exception('You must specify a database password.');
		}

		// Host
		$this->host = $input->getOption('host');
		if (is_null($this->host)) {
			throw new \Exception('You must specify a database host.');
		}

		// fullBackupsToKeep
		if (!is_null($input->getOption('fullBackupsToKeep'))) {
			$this->fullBackupsToKeep = $input->getOption('fullBackupsToKeep');
		}

		// fullBackupAgainAfter
		if (!is_null($input->getOption('fullBackupAgainAfter'))) {
			$this->fullBackupAgainAfter = $input->getOption('fullBackupAgainAfter');
		}
		$this->fullBackupAgainAfterSeconds = strtotime('+ ' . $this->fullBackupAgainAfter, 0);
		if ($this->fullBackupAgainAfterSeconds === false) {
			throw new \Exception('We could not understand the time period specified in fullBackupAgainAfter');
		}

		// Databases
		if (!is_null($input->getOption('databases'))) {
			$this->databases = $input->getOption('databases');
		}

		// Tool
		if (!is_null($input->getOption('tool'))) {
			$this->tool = $input->getOption('tool');
		}
        if (!in_array($this->tool, self::TOOL_OPTIONS)){
            throw new \Exception('Invalid tool specified, you can choose from the following: '.implode(', ', self::TOOL_OPTIONS).'.');
        }

        if (!is_null($input->getOption('ftwrl-wait-timeout'))) {
            $this->ftwrlWaitTimeout = $input->getOption('ftwrl-wait-timeout');
        }
	}

	private function shouldWeDoAFullBackup(): bool {
		$mostRecentTs = null;
		foreach (scandir(self::BACKUPS_DIR) as $dir) {
			// Check if this looks like a backup file
			if (preg_match('/^backup-([0-9]{4}-.+)$/', $dir, $matches)) {
				$this->io->write('Found backup directory ' . $dir);

				$thisBackupDirTs = strtotime($matches[1]);

				// It looked like one, but when we couldn't get a timestamp from it, so skip
				if ($thisBackupDirTs === false) {
					$this->io->writeln(', but could not extract a timestamp from it, so ignoring it.');
					continue;
				}

				if (is_null($mostRecentTs) || $thisBackupDirTs > $mostRecentTs) {
					$this->io->writeln(', this would appear to be the most recent found so far, from ' . $this->secondsToTime(time() - $thisBackupDirTs) . ' ago.');
					$mostRecentTs = $thisBackupDirTs;
					$this->mostRecentFullBackupDir = $dir;
				} else {
					$this->io->writeln(', but we\'ve already found a more recent one.');
				}
			}
		}

		return is_null($mostRecentTs) || ($mostRecentTs + $this->fullBackupAgainAfterSeconds) < time();
	}

	private function doFullBackup(): int {
		// Timestamp
		$backupTimeString = date('c');

		$this->io->title('Doing full backup at ' . $backupTimeString);

		// Work things out
		$backupDirectory = self::BACKUPS_DIR . '/backup-' . $backupTimeString . '/full';
		mkdir($backupDirectory, 0777, true);
		$this->io->writeLn('Backing up to "' . $backupDirectory . '"');

		// Build command
		$command = $this->tool.' --backup --target-dir=' . escapeshellarg($backupDirectory) . ' --user=' . escapeshellarg($this->user) . ' --password=' . escapeshellarg($this->password) . ' --host=' . escapeshellarg($this->host) . ' --datadir=' . escapeshellarg(self::MARIADB_DATA_DIR);

		// Optionally add $command
        if (!is_null($this->ftwrlWaitTimeout)) {
            $command .= ' --ftwrl-wait-timeout=' . escapeshellarg($this->ftwrlWaitTimeout);
        }

		// Optionally add databases
		if (!is_null($this->databases)) {
			$command .= ' --databases=' . escapeshellarg($this->databases);
		}

		// Run command
		passthru($command, $return_var);

		return $return_var;
	}

	private function doIncrementalBackup(): int {
		// Timestamp
		$backupTimeString = date('c');

		// Check we know where the full backup is
		if (is_null($this->mostRecentFullBackupDir)) {
			throw new \Exception('Cannot do incremental backup as do not have a full backup directory to base it on. This should not happen!');
		}

		$this->io->title('Doing incremental backup at ' . $backupTimeString . ', based on the full backup in ' . $this->mostRecentFullBackupDir);

		// Determine incrementals directory
		$incrementalsDir = self::BACKUPS_DIR . '/' . $this->mostRecentFullBackupDir . '/incr/';
		if (!file_exists($incrementalsDir)) {
			mkdir($incrementalsDir, 0777, true);
		}

		// Find the most recent incremental backup
		$mostRecentTs = null;
		$mostRecentDirName = null;
		foreach (scandir($incrementalsDir) as $dir) {
			// Check if this looks like a backup file
			if (preg_match('/^([0-9]{4}-.+)$/', $dir, $matches)) {
				$this->io->write('Found incremental backup directory ' . $dir);

				$thisBackupDirTs = strtotime($matches[1]);

				// It looked like one, but when we couldn't get a timestamp from it, so skip
				if ($thisBackupDirTs === false) {
					$this->io->writeln(', but could not extract a timestamp from it, so ignoring it.');
					continue;
				}

				if (is_null($mostRecentTs) || $thisBackupDirTs > $mostRecentTs) {
					$this->io->writeln(', this would appear to be the most recent found so far, from ' . $this->secondsToTime(time() - $thisBackupDirTs) . ' ago.');
					$mostRecentTs = $thisBackupDirTs;
					$mostRecentDirName = $dir;
				} else {
					$this->io->writeln(', but we\'ve already found a more recent one.');
				}
			}
		}

		// Work out where to base the backup from
		if (is_null($mostRecentTs)) {
			$baseDirectory = self::BACKUPS_DIR . '/' . $this->mostRecentFullBackupDir . '/full';
			$this->io->writeLn('Basing incremental backup on the full backup directory');
		} else {
			$baseDirectory = $incrementalsDir . $mostRecentDirName;
			$this->io->writeLn('Basing incremental backup on the incremental backup directory "' . $mostRecentDirName . '"');
		}

		// Work out where to put this backup
		$backupDirectory = $incrementalsDir . $backupTimeString;
		mkdir($backupDirectory, 0777, true);
		$this->io->writeLn('Backing up to "' . $backupDirectory . '"');

		// Build command
		$command = $this->tool.' --backup --target-dir=' . escapeshellarg($backupDirectory) . ' --incremental-basedir=' . escapeshellarg($baseDirectory) . ' --user=' . escapeshellarg($this->user) . ' --password=' . escapeshellarg($this->password) . ' --host=' . escapeshellarg($this->host) . ' --datadir=' . escapeshellarg(self::MARIADB_DATA_DIR);

		// Optionally add databases
		if (!is_null($this->databases)) {
			$command .= ' --databases=' . escapeshellarg($this->databases);
		}

		// Run command
		passthru($command, $return_var);

		return $return_var;
	}

	private function cleanup() {
		$this->io->title('Cleaning up old backups');

		// Determine all full backup directories
		$fullBackups = [];
		foreach (scandir(self::BACKUPS_DIR) as $dir) {
			// Check if this looks like a backup file
			if (preg_match('/^backup-([0-9]{4}-.+)$/', $dir, $matches)) {
				$this->io->write('Found backup directory ' . $dir);

				$thisBackupDirTs = strtotime($matches[1]);

				// It looked like one, but when we couldn't get a timestamp from it, so skip
				if ($thisBackupDirTs === false) {
					$this->io->writeln(', but could not extract a timestamp from it, so ignoring it.');
					continue;
				}

				// Add to array
				$fullBackups[$thisBackupDirTs] = $dir;
				$this->io->writeLn(' from ' . $this->secondsToTime(time() - $thisBackupDirTs) . ' ago.');
			}
		}

		if (count($fullBackups) > $this->fullBackupsToKeep) {
			// Sort array
			ksort($fullBackups);

			$backupsToPrune = count($fullBackups) - $this->fullBackupsToKeep;

			foreach ($fullBackups as $backupDir) {
				$this->io->writeln('Puring old full backup (and its incrementals) - '.$backupDir);

				$this->recursiveRmDir(self::BACKUPS_DIR.'/'.$backupDir);

				$backupsToPrune--;

				if ($backupsToPrune == 0) {
					break;
				}
			}
		} else {
			$this->io->writeln('We only have ' . count($fullBackups) . ' full backups so no need to cleanup.');
		}
	}

	private function secondsToTime($inputSeconds): string {
		$secondsInAMinute = 60;
		$secondsInAnHour = 60 * $secondsInAMinute;
		$secondsInADay = 24 * $secondsInAnHour;

		// Extract days
		$days = floor($inputSeconds / $secondsInADay);

		// Extract hours
		$hourSeconds = $inputSeconds % $secondsInADay;
		$hours = floor($hourSeconds / $secondsInAnHour);

		// Extract minutes
		$minuteSeconds = $hourSeconds % $secondsInAnHour;
		$minutes = floor($minuteSeconds / $secondsInAMinute);

		// Extract the remaining seconds
		$remainingSeconds = $minuteSeconds % $secondsInAMinute;
		$seconds = ceil($remainingSeconds);

		// Format and return
		$timeParts = [];
		$sections = [
			'day' => (int)$days,
			'hour' => (int)$hours,
			'minute' => (int)$minutes,
			'second' => (int)$seconds,
		];

		foreach ($sections as $name => $value) {
			if ($value > 0) {
				$timeParts[] = $value . ' ' . $name . ($value == 1 ? '' : 's');
			}
		}

		return implode(', ', $timeParts);
	}

	private function recursiveRmDir($dir) {
		if (is_dir($dir)) {
			$objects = scandir($dir);
			foreach ($objects as $object) {
				if ($object != '.' && $object != '..') {
					if (is_dir($dir . '/' . $object)) {
						$this->recursiveRmDir($dir . '/' . $object);
					} else {
						unlink($dir . '/' . $object);
					}
				}
			}
			rmdir($dir);
		}
	}
}
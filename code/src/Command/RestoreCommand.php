<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

class RestoreCommand extends Command {
	// Configurable options
    /** @var null|string $incremental */
	private $incremental = null;
	private $tool = 'mariabackup';

	const TOOL_OPTIONS = ['mariabackup', 'xtrabackup'];

	// Hardcoded options
	const BACKUPS_DIR = '/backups_data'; // Don't change this without checking the dockerfile and other commands
	const MARIADB_DATA_DIR = '/var/lib/mysql'; // Don't change this without checking the dockerfile and other commands

	/** @var SymfonyStyle */
	private $io = null;

	protected function configure() {
		$this
			->setName('restore')
			->setDescription('Restores a backup.')
            ->addOption('incremental', null, InputOption::VALUE_REQUIRED, 'Set which incremental backup to use')
			->addOption('tool', null, InputOption::VALUE_REQUIRED, 'Set the tool to be used for backups')
        ;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$this->io = new SymfonyStyle($input, $output);

		// Ensure the directory structure looks right
        if (! file_exists(self::BACKUPS_DIR.'/full')) {
            throw new \Exception('The backups full directory is missing. Have you mounted a backup into '.self::BACKUPS_DIR.'?');
        }
        if (! file_exists(self::BACKUPS_DIR.'/incr')) {
            throw new \Exception('The backups full directory is missing. Have you mounted a backup into '.self::BACKUPS_DIR.'?');
        }
        if (file_exists(self::BACKUPS_DIR.'/prepared')) {
            $this->recursiveRmDir(self::BACKUPS_DIR.'/prepared');
        }

		$this->io->title('Starting new restoration');

		$this->handleOptions($input);

		// Work out the incrementals to use
        if (! is_null($this->incremental)) {
            $incrementalBackupsToInclude = $this->getIncrementalBackupsToInclude();
            $this->io->title('Incremental backups to include');
            $this->io->listing($incrementalBackupsToInclude);
        }

		// Do the initial prepare
        if ($this->doPrepare() !== 0) {
            throw new \Exception('Initial prepare failed');
        }

		// Do any incremental prepares
        if (! is_null($this->incremental)) {
            // Loop though the directories in the right order
            foreach($incrementalBackupsToInclude as $incremental) {
                if ($this->doPrepare($incremental) !== 0) {
                    throw new \Exception('Incremental prepare of '.$incremental.' failed');
                }
            }
        }

        // Do the restore
        if ($this->doRestore() !== 0) {
            throw new \Exception('Restore failed');
        }

        $this->io->success('Backup restoration completed successfully!');
	}

	private function handleOptions(InputInterface $input) {
		// Incremental (can be null)
		$this->incremental = $input->getOption('incremental');
		if (! is_null($this->incremental)) {
		    if (! file_exists(self::BACKUPS_DIR.'/incr/'.$this->incremental)){
                throw new \Exception('The incremental backup "'.$this->incremental.'" could not be found');
            } else if (strtotime($this->incremental) === false) {
                throw new \Exception('The incremental backup "'.$this->incremental.'" was found but is not a point in time');
            }

        }

		// Tool
		if (!is_null($input->getOption('tool'))) {
			$this->tool = $input->getOption('tool');
		}
        if (!in_array($this->tool, self::TOOL_OPTIONS)){
            throw new \Exception('Invalid tool specified, you can choose from the following: '.implode(', ', self::TOOL_OPTIONS).'.');
        }
	}

	private function getIncrementalBackupsToInclude(): array {
	    $this->io->title('Working out the incremental backups to include');

	    $incrementalBackupsToInclude = [];
	    $incrementalTs = strtotime($this->incremental);
        $incrementalBackupsToInclude[$incrementalTs] = $this->incremental;

	    foreach (scandir(self::BACKUPS_DIR.'/incr') as $object) {
	        $this->io->write('Found '.$object.'… ');
            if ($object != '.' && $object != '..' && is_dir(self::BACKUPS_DIR.'/incr/'.$object)) {
                $dirTs = strtotime($object);
                if ($dirTs === false) {
                    throw new \Exception('Unable to handle dir '.$object.' as it does not look to be a point in time');
                }

                if ($dirTs < $incrementalTs) {
                    $incrementalBackupsToInclude[$dirTs] = $object;
                    $this->io->writeLn('including');
                } else {
                    $this->io->writeLn('excluding as after our final one');
                }
            } else {
                $this->io->writeLn('but skipping as not valid directory');
            }
        }

        ksort($incrementalBackupsToInclude);

	    return $incrementalBackupsToInclude;
    }

    private function doPrepare(?string $incremental = null) {
	    // Determine the initial bases of the command
        if (is_null($incremental)) {
            $this->io->title('Starting full preparation');

            $this->io->writeln('Copying full backup into prepared directory.');
            passthru('cp -R '.escapeshellarg(self::BACKUPS_DIR.'/full').' '.escapeshellarg(self::BACKUPS_DIR.'/prepared'), $return_var);
            if ($return_var === 0) {
                $this->io->writeln('Copying completed ok');
            } else {
                $this->io->writeln('Copying failed');
                throw new \Exception('Copying from full to prepared directory failed');
            }

            $command = $this->tool.' --prepare --apply-log-only --target-dir='.escapeshellarg(self::BACKUPS_DIR.'/prepared');
        } else {
            $this->io->title('Starting incremental preparation of "'.$incremental.'"');
            $command = $this->tool.' --prepare --apply-log-only --target-dir='.escapeshellarg(self::BACKUPS_DIR.'/prepared').' --incremental-dir='.escapeshellarg(self::BACKUPS_DIR.'/incr/'.$incremental);
        }

        // Run command
        passthru($command, $return_var);

        if ($return_var === 0) {
            $this->io->success('Prepare completed ok :-)');
        } else {
            $this->io->error('Prepare command failed :-(');
        }

        return $return_var;
    }

    private function doRestore() {
        $this->io->title('Starting restoration of backup');

        $command = $this->tool.' --move-back --target-dir='.escapeshellarg(self::BACKUPS_DIR.'/prepared') . ' --datadir=' . escapeshellarg(self::MARIADB_DATA_DIR);

        // Run command
        passthru($command, $return_var);

        if ($return_var === 0) {
            $this->io->success('Prepare completed ok :-)');
        } else {
            $this->io->error('Prepare command failed :-(');
        }

        return $return_var;
    }

    private function recursiveRmDir(string $dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != '.' && $object != '..') {
                    if (is_dir($dir . '/' . $object)) {
                        $this->recursiveRmDir($dir . '/' . $object);
                    } else {
                        unlink($dir . '/' . $object);
                    }
                }
            }
            rmdir($dir);
        }
    }
}
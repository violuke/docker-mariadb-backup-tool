FROM php:7.3-cli
MAINTAINER Luke Cousins

# Add MariaDB repository
RUN apt-get update -yqq
RUN apt-get install -yqq software-properties-common dirmngr git
RUN APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1 apt-key adv --no-tty --recv-keys --keyserver keyserver.ubuntu.com 0xF1656F24C74CD1D8
RUN APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1 add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://mirrors.coreix.net/mariadb/repo/10.5/debian buster main'
RUN apt-get update -yqq

# Install mariabackup (unfortinuately this installs the actual db server too, so we nee the password!)
RUN echo mysql-server mysql-server/root_password password "somepassword" | debconf-set-selections; \
echo mysql-server mysql-server/root_password_again password "somepassword" | debconf-set-selections; \
apt-get install -yqq mariadb-backup

# Install xtrabackup
# Disabling due to issue on debian buster
#RUN curl -O https://repo.percona.com/apt/percona-release_0.1-6.$(lsb_release -sc)_all.deb
#RUN dpkg -i percona-release_0.1-6.$(lsb_release -sc)_all.deb
#RUN apt-get update -yqq
#RUN apt-get install -yqq percona-xtrabackup-24

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Display Composer version
RUN composer --version

# Time Zone
RUN echo "date.timezone=Europe/London" > $PHP_INI_DIR/conf.d/date_timezone.ini

# Display PHP version
RUN php --version # This fails due to the auto_prepend_file being missing at this stage

WORKDIR /root

# This is not expected to change and should match what the PHP scripts expect
VOLUME /backups_data
VOLUME /var/lib/mysql/

# Copy in our backup script
COPY code /root/code
RUN chmod 777 /root/code/Run.php
RUN cd /root/code && composer install


ENTRYPOINT ["php", "/root/code/Run.php"]
